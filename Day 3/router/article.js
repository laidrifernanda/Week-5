const router = require("express").Router();

const articleController = require("../controllers/articleController");

router.get("/:path", articleController.home);

module.exports = router;
