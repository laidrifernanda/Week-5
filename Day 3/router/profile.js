const router = require("express").Router();
const profileController = require("../controllers/profileController");

router.get("/", profileController.home);

router.get("/all", profileController.all);

router.get("/find/:number", profileController.find);

router.get("/artikel/:id?", (req, res) => {
  const id = req.params.id ? req.params.id : "";
  res.send("Halaman artikel dari halaman profile " + id);
});

router.post("/news", (req, res) =>
  res.send("halaman news dari halaman profile")
);

module.exports = router;
