class ArticleController {
  home(req, res) {
    switch (req.params.path) {
      case "all":
        return res.send("Halaman All");
      case "find":
        return res.send("Halaman find article");
      case "search":
        return res.send("Halaman search article");
      default:
        res.send("default article");
        break;
    }
  }
}

module.exports = Object.freeze(new ArticleController());
