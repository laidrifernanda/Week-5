const profileModel = require("../model/profileModel");

class ProfileController {
  home(req, res) {
    res.send("halaman home dari halaman profile");
  }
  all(req, res) {
    const listProfile = profileModel.getAllProfile();
    res.render(__dirname + "/view/profile/all.ejs", { listProfile });
  }

  find(req, res) {
    const findProfile = profileModel.getFindProfile(req.params.number - 1);
    res.json(findProfile);
  }

  news(req, res) {
    res.send("Halaman news dari page profile");
  }

  artikel(req, res) {
    const id = req.params.id ? req.params.id : "";
    res.send("Halaman artikel dari page profile " + id);
  }
}

module.exports = Object.freeze(new ProfileController());
