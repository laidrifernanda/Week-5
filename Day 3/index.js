const express = require("express");

const app = express();

app.set("view engine", "ejs");

const profileRouter = require("./router/profile");
const articleRouter = require("./router/article");

app.get("/", (req, res) => res.send("Halaman Home"));

app.use("/profile", profileRouter);

app.use("/article", articleRouter);

// all
// find
// search

app.listen(4000, (err) => console.log("running on port 4000"));
