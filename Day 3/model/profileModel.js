class ProfileModel {
  #dataProfile;
  constructor() {
    this.dataProfile = [
      {
        id: 1,
        name: "Muhammad Laidri Fernanda",
        jk: "Laki-laki",
      },
      {
        id: 2,
        name: "Hahaha",
        jk: "Laki-laki",
      },
      {
        id: 3,
        name: "Hihihi",
        jk: "Perempuan",
      },
      {
        id: 4,
        name: "Bweeeee",
        jk: "Laki-laki",
      },
    ];
  }
  getAllProfile() {
    return this.#dataProfile;
  }
  getFindProfile(number) {
    return this.dataProfile[number];
  }
}

module.exports = Object.freeze(new ProfileModel());
