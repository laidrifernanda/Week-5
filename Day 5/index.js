const express = require("express");

const app = express();

//routing user
//create user
app.post("/users", (req, res) => {
  res.send("routing untuk create users");
});
//read user
app.get("/users", (req, res) => {
  res.send("routing untuk read users");
});

//update user
app.put("/users", (req, res) => {
  res.send("routing untuk update users");
});

//delete user
app.delete("/users", (req, res) => {
  res.send("routing untuk delete user");
});

//routing tasks
//create tasks
app.post("/tasks", (req, res) => {
  res.send("routing untuk create users");
});
//read tasks
app.get("/tasks", (req, res) => {
  res.send("routing untuk read users");
});

//update tasks
app.put("/tasks", (req, res) => {
  res.send("routing untuk update users");
});

//delete tasks
app.delete("/tasks", (req, res) => {
  res.send("routing untuk delete user");
});

app.listen(4000, () => console.log("app running on port 4000"));
