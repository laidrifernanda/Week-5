const express = require("express");

const app = express();

app.get("/", (req, res) => {
  res.send("hello from express");
});

app.get("/showbiz", (req, res) => {
  res.sendFile(__dirname + "/html/showbiz.html");
});

app.get("/news", (req, res) => {
  res.sendFile(__dirname + "/html/news.html");
});

app.get("/life", (req, res) => {
  res.sendFile(__dirname + "/html/life.html");
});

app.get("/hotissue", (req, res) => {
  res.sendFile(__dirname + "/html/hotissue.html");
});

app.get("/sports", (req, res) => {
  res.sendFile(__dirname + "/html/sports.html");
});

app.get("/campus", (req, res) => {
  res.sendFile(__dirname + "/html/campus.html");
});

app.get("/games", (req, res) => {
  res.sendFile(__dirname + "/html/games.html");
});

app.get("/covid19", (req, res) => {
  res.sendFile(__dirname + "/html/covid 19.html");
});

app.get("/otomotif", (req, res) => {
  res.sendFile(__dirname + "/html/otomotif.html");
});

app.listen(4000, () => console.log("app running on port 4000"));
