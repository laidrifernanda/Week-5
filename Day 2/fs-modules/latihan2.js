// const fs = require("fs");

// fs.write("fernanda.json", (err) => {
//   if (err) return console.log(err);
// });

// Include fs module
const fs = require("fs");

const str = "Hello world";
const filename = "input.txt";

fs.open(filename, "a", (err, fd) => {
  if (err) {
    console.log(err.message);
  } else {
    fs.write(fd, str, (err, bytes) => {
      if (err) {
        console.log(err.message);
      } else {
        console.log(bytes + " bytes written");
      }
    });
  }
});
